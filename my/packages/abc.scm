(define-module (my packages abc)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses)
  #:use-module (guix utils))

(define-public abcm2ps
  (package
   (name "abcm2ps")
   (version "v8.14.6")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/leesavide/abcm2ps")
		  (commit version)))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32 "1gqjqbd8wj0655vi8gcg2r5jqzafdlnfjzwa9z331ywhrskpm53w"))))
   (build-system gnu-build-system)
   (arguments
    '(#:tests? #f
      #:make-flags
      (list (string-append "prefix=" %output))
      #:phases (modify-phases %standard-phases
			      (add-after 'patch-source-shebangs 'patch-install-program
					 ;; Replace the hard coded
					 ;; install program in the
					 ;; configure script.
					 (lambda _
					   (substitute* "configure"
							(("/usr/bin/install") (which "install"))))
					 ))))
   (synopsis "Convert ABC notation to music sheet in ps or svg.  (Extension of abc2ps)")
   (description "abcm2ps is a command line program which converts ABC to music sheet in PostScript or SVG format.  It is an extension of abc2ps which may handle many voices per staff.  abcm2ps is Copyright © 2014-2016 Jean-Francois Moine.  http://moinejf.free.fr/")
   (home-page "http://moinejf.free.fr/")
   (license gpl3+)))
