(define-module (my packages seafile-client)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages web)
  #:use-module (gnu packages tls))

(define-public libsearpc
  (package
   (name "libsearpc")
   (version "3.1")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/haiwen/libsearpc")
		  (commit (string-append "v" version "-latest"))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32 "078r0bgncgadsv4v82kxmq2mvd19grssakjn0syrv61a96vmfyjj"))))
   (build-system gnu-build-system)
   (inputs `(("glib" ,glib)
	     ("jansson" ,jansson)
	     ("python" ,python)
	     ("python-simplejson" ,python-simplejson)))
   (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("libtool" ,libtool)
      ("gobject-introspection" ,gobject-introspection)
      ("pkg-config" ,pkg-config)))
   (synopsis "Simple C language RPC framework based on GObject system.")
   (description "Searpc is a simple C language RPC framework based on
		 GObject system.  Searpc handles the serialization/deserialization part
		 of RPC, the transport part is left to users.")
   (home-page "https://github.com/haiwen/libsearpc")
   (license license:asl2.0)))

(define-public seafile-shared
  (package
   (name "seafile-shared")
   (version "8.0.1")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/haiwen/seafile")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32 "047dnwm49j949xgc3nvyn1y5p03bf0wk3zj58cvmh34kf2phdajl"))))
   (build-system gnu-build-system)
   (inputs `(("libcurl" ,curl)
	     ("sqlite3" ,sqlite)
	     ("jansson" ,jansson)
	     ("libevent" ,libevent)
	     ("libsearpc" ,libsearpc)
	     ("python" ,python)
	     ("openssl" ,openssl)))
   (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("libtool" ,libtool)
      ("pkg-config" ,pkg-config)
      ("vala" ,vala)))
   (synopsis "Shared components of Seafile")
   (description "Shared components of Seafile: seafile-daemon, libseafile, libseafile python bindings, manuals, and icons")
   (home-page "https://github.com/haiwen/seafile")
   (license license:gpl2)))

(define-public seafile-client
  (package
   (name "seafile-client")
   (version "8.0.0")
   (source (origin
	    (method git-fetch)
	    (uri (git-reference
		  (url "https://github.com/haiwen/seafile-client")
		  (commit (string-append "v" version))))
	    (file-name (git-file-name name version))
	    (sha256
	     (base32 "1f5p36xy6r16fsyswjm04l15nn292wvlqwq5vm06vin3kgy7pix8"))))
   (build-system cmake-build-system)
   ;; FIXME: Figure out if there are tests.
   (arguments '(#:tests? #f))
   (inputs `(("qtbase" ,qtbase)
	     ("glib" ,glib)
	     ("sqlite3" ,sqlite)
	     ("jansson" ,jansson)
	     ("openssl" ,openssl)
	     ("libsearpc" ,libsearpc)
	     ("libevent" ,libevent)))
   (native-inputs
    `(("pkg-config" ,pkg-config)
      ("qtlinguist" ,qttools)
      ("gobject-introspection" ,gobject-introspection)
      ("pkg-config" ,pkg-config)))
   (propagated-inputs
    `(("seafile-shared" ,seafile-shared)))
   (synopsis "Client for the seafile cloud sharing program")
   (description "Graphical client for seafile.")
   (home-page "https://www.seafile.org")
   (license license:asl2.0)))
