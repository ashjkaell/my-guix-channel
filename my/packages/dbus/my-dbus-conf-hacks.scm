(define-module (my packages dbus my-dbus-conf-hacks)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module (guix licenses))

(define-public my-dbus-conf-hacks
  (package
  (name "my-dbus-conf-hacks")
  (version "2")
  (source (origin
	   (method url-fetch)
	   (uri "file:///home/gabriel/Programs/guile/my-guix-channel/my/packages/dbus")	;TODO: Replace this with git link
	   (sha256
	    (base32 "1gqjqbd8wj0655vi8gcg2r5jqzafdlnfjzwa9z331ywhrskpm53w"))))
  (build-system trivial-build-system)
  (arguments '(#:modules ((guix build utils))
	       #:builder
	       (begin
		 (use-modules (guix build utils))
		 (let ((source (assoc-ref %build-inputs "source")))
		   (copy-recursively (string-append source "/hacks") (string-append %output "/etc/dbus-1/system.d/"))
		   #t))))
  (synopsis "Personal modifications to dbus security rules.")
  (description "This package does nothing apart from copying files in  the store TODO")
  (license gpl3+)
  (home-page "nil")))
